// REST API
//
// Application Programming Interface
// The part of a server responsible for receiving request and sending responses.
// Hides the server beneath an interface layer
// 		Hidden complexity makes apps easier to use
// 		Sets the rules of interaction between front and back ends of an application, improving security
// 
// What is REST
// Representational State Transfer
// An architectural style for providing standards for communication between computer systems.
// 
// Statelessness - Server does not need to know client state and vice versa
// 
// Standardized Communication - Enables a decoupled server to understand, process, and respond to client requests WITHOUT knowing client state
// Implemented via resources represented as uniform resource identifier(URI) endpoints and HTTP methods
// 		Resources are plural by convention
// 		Ex. /api/photos(NEVER/api/photo)
// 			
// Anatomy of a Client Request
// 		Operation to be perforemd dictated by HTTP methods
//
//A path to the resource to be operated on
//		URI endpoint
//
//A header containing addtional request information
//		A request body containing data (optional)
//
//
//